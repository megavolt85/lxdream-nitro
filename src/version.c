
#include <projconfig.h>

const char lxdream_package_name[] = PACKAGE PACKAGE_VERSION;
const char lxdream_short_version[] = PACKAGE_VERSION;
const char lxdream_full_version[] = PACKAGE_VERSION;
const char lxdream_copyright[] = "Copyright (C) 2005-2008 Nathan Keynes. Copyright (C) 2019 Luke Benstead";

